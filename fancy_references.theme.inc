<?php

/**
 * Theme functions for Fancy References.
 */

/**
 * Theme function for the node reference formatter.
 */
function theme_fancy_references_nodereference($element) {
  static $recursion_queue = array();
  $output = '';
  if (!empty($element['#item']['safe']['nid'])) {
    $nid = $element['#item']['safe']['nid'];
    $node = $element['#node'];
    $field = content_fields($element['#field_name'], $element['#type_name']);
    // If no 'referencing node' is set, we are starting a new 'reference thread'
    if (!isset($node->referencing_node)) {
      $recursion_queue = array();
    }
    $recursion_queue[] = $node->nid;
    if (in_array($nid, $recursion_queue)) {
      // Prevent infinite recursion caused by reference cycles:
      // if the node has already been rendered earlier in this 'thread',
      // we fall back to 'default' (node title) formatter.
      return theme('nodereference_formatter_default', $element);
    }
    if ($referenced_node = node_load($nid)) {
      fancy_references_process_string($pattern, $referenced_node);
      
      $output = fancy_references_process_element($element);
      // Display the values.
      $output = $node->title;
    }
  }
  return $output;
}

function theme_fancy_references_formatter_userreference($element) {
  $output = '';
  if (isset($element['#item']['uid']) && $account = user_load(array('uid' => $element['#item']['uid']))) {
    $output = fancy_references_process_element($element);
    //$output = theme('username', $account);
  }
  return $output;
}
